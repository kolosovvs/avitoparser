from dataclasses import dataclass
from decimal import Decimal
from typing import Optional, Dict


@dataclass
class Advert:
    name: str
    url: str
    price: Optional[Decimal]
    metro_station: Optional[str]
    meters_from_metro: Optional[Decimal]

    def to_dict(self) -> Dict:
        return {
            'name': self.name,
            'url': self.url,
            'price': self.price,
            'metro_station': self.metro_station,
            'meters_from_metro': self.meters_from_metro
        }
