from decimal import Decimal
from typing import List, Optional
from bs4 import BeautifulSoup, Tag
import requests
from pandas import DataFrame
from src.dto import Advert


class AvitoParser:
    url = 'https://www.avito.ru/moskva'
    root_url = 'https://www.avito.ru'

    def __init__(self, query: str, page: int):
        self.query = query
        self.page = page

    def get_html(self) -> str:
        payload = {
            'q': self.query,
            'p': self.page
        }
        response = requests.get(self.url, params=payload)
        return response.content.decode("utf-8")

    def get_ad_name(self, item: Tag) -> str:
        return item.find("div", {"class": "snippet-title-row"}).text

    def get_ad_price(self, item: Tag) -> Optional[Decimal]:
        dirty_str_price = item.find("div", {"class": "snippet-price-row"}).text
        if "бесплатно" in dirty_str_price.lower():
            return Decimal(0)
        if "цена не указана" in dirty_str_price.lower():
            return None
        str_price = ''.join(char for char in dirty_str_price if char.isdigit())
        return Decimal(str_price)

    def get_ad_url(self, item: Tag) -> str:
        path = item.find("a", {"class": "snippet-link"}).attrs.get('href')
        return f"{self.root_url}{path}"

    def get_ad_metro_station(self, item: Tag) -> Optional[str]:
        try:
            return item.find(
                "span",
                {"class": "item-address-georeferences-item__content"}).text
        except AttributeError:
            return None

    def get_ad_meters_from_metro(self, item: Tag) -> Optional[Decimal]:
        try:
            dirty_str_meters = item.find(
                "span",
                {"class": "item-address-georeferences-item__after"}).text

            str_meters = \
                ''.join(char for char in dirty_str_meters if char.isdigit())
            str_meters = Decimal(str_meters)
        except AttributeError:
            str_meters = None
        return str_meters

    def parse(self) -> List[Advert]:
        html_doc = self.get_html()
        soup = BeautifulSoup(html_doc, 'html.parser')
        ad_list = []
        for item in soup.findAll("div", {"class": "item_table"}):
            advert = Advert(
                name=self.get_ad_name(item),
                url=self.get_ad_url(item),
                price=self.get_ad_price(item),
                metro_station=self.get_ad_metro_station(item),
                meters_from_metro=self.get_ad_meters_from_metro(item)
            )
            ad_list.append(advert)
        return ad_list

    # pylint: disable=invalid-name
    def get_data_frame(self) -> DataFrame:
        data = self.parse()
        df = DataFrame.from_records([adv.to_dict() for adv in data])
        return df

    def save_to_csv(self, file_name: str, df: DataFrame):
        df.to_csv(file_name)
