from src.avito_parser import AvitoParser


# pylint: disable=invalid-name
def parse_page(search: str, page: int):
    avito_parser = AvitoParser(query=search, page=page)
    df = avito_parser.get_data_frame()
    print(df)


if __name__ == "__main__":
    parse_page("гречка", 1)
